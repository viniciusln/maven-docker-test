FROM maven:3-jdk-8-alpine

# criando um maven repo para ser persistente entre os passos da build
RUN mkdir -p /usr/maven_repo

# primeira parte, download das dependências
RUN mkdir -p /usr/tmp
WORKDIR /usr/tmp
# aqui eu adiciono apenas o pom.xml para criar uma camada (cache) da imagem que será recriada apenas se houver mudança no pom
# ou seja, se o código mudar mas o pom não, o cache das dependências será reaproveitado
ADD pom.xml /usr/tmp/pom.xml
RUN mvn -Dmaven.repo.local=/usr/maven_repo dependency:go-offline
# estou explicitamente ignorando o código de retorno porque o mvn install vai acontecer mesmo depois
# por enquanto somente preciso fazer caches para evitar fazer download de tudo a cada rebuild
RUN mvn -Dmaven.repo.local=/usr/maven_repo install; exit 0

# traz o resto do código e vamos compilar pra valer agora
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD . /usr/src/app
RUN cp /usr/tmp/pom.xml /usr/src/app/pom.xml
RUN mvn -Dmaven.repo.local=/usr/maven_repo clean install

# finalmente, executa
CMD ["/usr/bin/java","-jar","/usr/src/app/target/final.jar"]
