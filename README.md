Experimento com maven e docker para reaproveitar o maven_repo criando cache da imagem

Execute o seguinte comando:
docker build . -t test && docke run test

Na primeira vez vai demorar uma eternidade porque está baixando todo o maven_repo.
Altere a classe Main e execute o comando novamente, será bem mais rápido pois não vai baixar tudo de novo.

Experimente alterar o pom. Vai demorar muito novamente pois a camada do cache do pom será inválidada e será necessário baixar tudo de novo.



